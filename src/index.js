import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import LoginControl from './LoginControl';
import reportWebVitals from './reportWebVitals';
import Mailbox from './Mailbox';
import Page from './Page';

const messages = ['React', 'Re:React','Re:Re:React' ,'re'];
//const messages = [];
ReactDOM.render(
 //<LoginControl />, --------------------------1
// <Mailbox unreadMessages = {messages} />------2
<Page />,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
